package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//  An Interface contains behavior that a class implements
//  @Repository - contains methods for database manipulation
@Repository

//  By extending CrudRepository, PostRepository has inherited its pre-defined methods for creating, retrieving, updating and deleting records
public interface PostRepository extends CrudRepository<Post, Object> {







}
